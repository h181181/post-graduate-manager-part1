﻿using System.Data.Entity;

namespace Post_Graduate_Manager_Part1.Model
{
    class PostGraduateManagerPart1DBContext : DbContext
    {
        public DbSet<Supervisor> Supervisors { get; set; }
        public DbSet<Student> Students { get; set; }

    }
}
