﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace Post_Graduate_Manager_Part1.Model
{
    class Student : DbContext
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public Supervisor Supervisor { get; set; }
        public int SupervisorId { get; set; }
    }
}
