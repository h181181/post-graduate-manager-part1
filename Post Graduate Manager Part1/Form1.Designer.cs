﻿namespace Post_Graduate_Manager_Part1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dvgManager = new System.Windows.Forms.DataGridView();
            this.comboBoxSupervisors = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxStudentAdd = new System.Windows.Forms.TextBox();
            this.addStudentBtn = new System.Windows.Forms.Button();
            this.deleteStudentBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.serializedBox = new System.Windows.Forms.TextBox();
            this.serializeBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dvgManager)).BeginInit();
            this.SuspendLayout();
            // 
            // dvgManager
            // 
            this.dvgManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgManager.Location = new System.Drawing.Point(332, 77);
            this.dvgManager.MultiSelect = false;
            this.dvgManager.Name = "dvgManager";
            this.dvgManager.RowHeadersWidth = 51;
            this.dvgManager.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvgManager.Size = new System.Drawing.Size(399, 277);
            this.dvgManager.TabIndex = 0;
            // 
            // comboBoxSupervisors
            // 
            this.comboBoxSupervisors.FormattingEnabled = true;
            this.comboBoxSupervisors.Location = new System.Drawing.Point(36, 153);
            this.comboBoxSupervisors.Name = "comboBoxSupervisors";
            this.comboBoxSupervisors.Size = new System.Drawing.Size(169, 24);
            this.comboBoxSupervisors.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Supervisors:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Student name:";
            // 
            // textBoxStudentAdd
            // 
            this.textBoxStudentAdd.Location = new System.Drawing.Point(36, 234);
            this.textBoxStudentAdd.Name = "textBoxStudentAdd";
            this.textBoxStudentAdd.Size = new System.Drawing.Size(169, 22);
            this.textBoxStudentAdd.TabIndex = 4;
            // 
            // addStudentBtn
            // 
            this.addStudentBtn.Location = new System.Drawing.Point(36, 284);
            this.addStudentBtn.Name = "addStudentBtn";
            this.addStudentBtn.Size = new System.Drawing.Size(169, 39);
            this.addStudentBtn.TabIndex = 5;
            this.addStudentBtn.Text = "Add student";
            this.addStudentBtn.UseVisualStyleBackColor = true;
            this.addStudentBtn.Click += new System.EventHandler(this.AddStudentBtn_Click);
            // 
            // deleteStudentBtn
            // 
            this.deleteStudentBtn.Location = new System.Drawing.Point(332, 400);
            this.deleteStudentBtn.Name = "deleteStudentBtn";
            this.deleteStudentBtn.Size = new System.Drawing.Size(166, 38);
            this.deleteStudentBtn.TabIndex = 6;
            this.deleteStudentBtn.Text = "Delete student";
            this.deleteStudentBtn.UseVisualStyleBackColor = true;
            this.deleteStudentBtn.Click += new System.EventHandler(this.DeleteStudentBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(275, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Add students and choose their supervisor:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(366, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Student table:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(332, 377);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(360, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Choose a student and click the delete button to remove:";
            // 
            // serializedBox
            // 
            this.serializedBox.Location = new System.Drawing.Point(821, 168);
            this.serializedBox.Multiline = true;
            this.serializedBox.Name = "serializedBox";
            this.serializedBox.Size = new System.Drawing.Size(220, 142);
            this.serializedBox.TabIndex = 10;
            // 
            // serializeBtn
            // 
            this.serializeBtn.Location = new System.Drawing.Point(821, 109);
            this.serializeBtn.Name = "serializeBtn";
            this.serializeBtn.Size = new System.Drawing.Size(220, 40);
            this.serializeBtn.TabIndex = 12;
            this.serializeBtn.Text = "Serialize";
            this.serializeBtn.UseVisualStyleBackColor = true;
            this.serializeBtn.Click += new System.EventHandler(this.SerializeBtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(821, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(225, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Supervisors objects to JSON form:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 475);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.serializeBtn);
            this.Controls.Add(this.serializedBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.deleteStudentBtn);
            this.Controls.Add(this.addStudentBtn);
            this.Controls.Add(this.textBoxStudentAdd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxSupervisors);
            this.Controls.Add(this.dvgManager);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dvgManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dvgManager;
        private System.Windows.Forms.ComboBox comboBoxSupervisors;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxStudentAdd;
        private System.Windows.Forms.Button addStudentBtn;
        private System.Windows.Forms.Button deleteStudentBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox serializedBox;
        private System.Windows.Forms.Button serializeBtn;
        private System.Windows.Forms.Label label6;
    }
}

