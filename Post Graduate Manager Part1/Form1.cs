﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Post_Graduate_Manager_Part1.Model;

namespace Post_Graduate_Manager_Part1
{
    public partial class Form1 : Form
    {
        // this must be above form constructor
        private PostGraduateManagerPart1DBContext PostGraduateContext;
  
        public Form1()
        {
            InitializeComponent();

            //this object represents the current state of the DB
            PostGraduateContext = new PostGraduateManagerPart1DBContext();

            List<Supervisor> supervisors = PostGraduateContext.Supervisors.ToList();
            foreach (Supervisor item in supervisors)
            {
                //add each to some component
                comboBoxSupervisors.Items.Add(item);
            }

            UpdateGridView();
        }

        private void AddStudentBtn_Click(object sender, EventArgs e)
        {
            //new object for EF to use in a DBSet
            Student studentObj = new Model.Student
            {
                Name = textBoxStudentAdd.Text,
                SupervisorId = ((Supervisor)comboBoxSupervisors.SelectedItem).Id
                
                
            };
            PostGraduateContext.Students.Add(studentObj);
            PostGraduateContext.SaveChanges();
            UpdateGridView();
        }
        private void DeleteStudentBtn_Click(object sender, EventArgs e)
        {
            // find the object in the DBSet you want to delete
            Student tempStudentObj = PostGraduateContext.Students.Find(dvgManager.SelectedRows[0].Cells[0].Value); 
            //Remove it
            PostGraduateContext.Students.Remove(tempStudentObj);
            PostGraduateContext.SaveChanges();

            UpdateGridView();
        }

        private void UpdateGridView()
        {
            // this allows us to tie a DGV to a given source
            BindingSource myBindingSource = new BindingSource();
            dvgManager.DataSource = myBindingSource;

            //Use LINQ to edit the results for the DGV
            var resultSet = from row in PostGraduateContext.Students select new
            {
                row.Id,
                row.Name,
                row.Supervisor,
                row.SupervisorId
            };

            myBindingSource.DataSource = resultSet.ToList(); 
            dvgManager.Refresh();
        }

        private void SerializeBtn_Click(object sender, EventArgs e)
        {
            //serialize supervisors from resultSet to JSON
            var resultSet = from row in PostGraduateContext.Supervisors
                            select new
                            {
                                row.Id,
                                row.Name,      
                            };
            serializedBox.Text = JsonConvert.SerializeObject(resultSet).ToString();
        }
    }
}
