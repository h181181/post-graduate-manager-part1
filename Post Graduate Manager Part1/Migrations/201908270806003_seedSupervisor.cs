namespace Post_Graduate_Manager_Part1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedSupervisor : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Supervisors (Name) VALUES('Dean')");
            Sql("INSERT INTO Supervisors (Name) VALUES('Greg')");
            Sql("INSERT INTO Supervisors (Name) VALUES('Peter')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Supervisors WHERE Name = 'Dean'");
            Sql("DELETE FROM Supervisors WHERE Name = 'Greg'");
            Sql("DELETE FROM Supervisors WHERE Name = 'Peter'");
        }
    }
}
